---
author: Votre nom
title: 📝 Tutoriels
---

# 📝 Tutoriel `mardown` et `mkdocs-material`

On présente ci-dessous différentes fonctionnalités offertes par `markdown` et `mkdocs-material`.

Déroulez chaque bloc pour avoir des exemples.


??? abstract "Titres"
    
    Il est très simple de créer des titres en `markdown`.

    === "Markdown"

        ```title=""
        # Titre 1

        ## Sous-titre 1
        
        ### Sous-sous-titre 1
        ```

    === "Rendu"

        # Titre 1

        ## Sous-titre 1
        
        ### Sous-sous-titre 1

??? abstract "Mise en forme du texte"

    La mise en forme du texte est rapide.

    === "Markdown"

        ```title=""
        *Texte en italique*

        **Texte en gras**
        
        ***Texte en gras italique***

        ~~Texte barré~~
        ```

    === "Rendu"

        *Texte en italique*

        **Texte en gras**

        ***Texte en gras italique***

        ~~Texte barré~~

??? abstract "Liens hypertexte"

    On insère des textes en utilisant la syntaxe `[texte du lien](adresse à visiter)`.

    === "Markdown"

        ```title=""
        Voici un texte avec un [lien](https://www.markdownguide.org/basic-syntax/).
        ```

    === "Rendu"

        Voici un texte avec un [lien](https://www.markdownguide.org/basic-syntax/).

??? abstract "Images"

    L'insertion des images ressemble à celles des liens `![texte alternatif](adresse de l'image)`. Notez le `!` avant le premier crochet.

    === "Markdown"

        ```title=""
        ![Un renard](images/renard.jpg){ width=50% }
        
        Cette image est dans le sous-dossier `images` à partir de l'emplacement courant.
        ```

    === "Rendu"

        ![Un renard](images/renard.jpg){ width=50% }
        
        Cette image est dans le sous-dossier `images` à partir de l'emplacement courant.

??? abstract "Admonitions"

    `mkdocs-material` permet de créer facilement des blocs mis en évidence appelés **admonitions**

    On présente ci-dessous un exemple général avec une admonition de type `note` :

    === "Markdown"

        ```title=""
        !!! note "Titre de la note"

            Contenu de la note. Ce texte est indenté.

            Vous pouvez changer le titre mais **pas** le mot-clé `note` qui défini le format de l'admonition.
        ```

    === "Rendu"

        !!! note "Titre de la note"

            Contenu de la note. Ce texte est indenté

            Vous pouvez changer le titre mais **pas** le mot-clé `note` qui défini le format de l'admonition.
            
    Il existe d'autres types d'admonitions. Leur utilisation est identique à celle de `note`, il suffit de remplacer `note` par le type choisi.

    Dans les exemples ci-dessous, afin d'alléger la page, on n'a pas ajouté de texte dans l'admonitions.

    !!! abstract "`abstract`"
    
    !!! question "`question`"

    !!! example "`example`"
    
    !!! tip "`tip`"

    !!! warning "`warning`"

    !!! danger "`danger`"


??? abstract "Admonitions déroulantes"

    Il est possible de créer des admonitions déroulantes en utilisant la même syntaxe que pour les admonitions.
    
    On remplace juste les `!!!` par des `???`.

    Par défaut, l'admonition est fermée. En saisissant `???+`, elle est ouverte par défaut

    === "Markdown"

        ```title=""
        ??? note "Admonition déroulante fermée par défaut"

            Contenu de la note.

        ???+ note "Admonition déroulante ouverte par défaut"

            Contenu de la note. Notez le `+` après les points d'interrogation.
        ```

    === "Rendu"

        ??? note "Admonition déroulante fermée par défaut"

            Contenu de la note.

        ???+ note "Admonition déroulante ouverte par défaut"

            Contenu de la note. Notez le `+` après les points d'interrogation.

??? abstract "Listes"

    Pour écrire une liste non-ordonnée, on introduit chaque *item* par un `*` (ou `-`, au choix).

    === "Markdown"

        ```title=""
        Texte précédant la liste. Notez la ligne blanche avant le début de la liste.

        * premier *item*
        * deuxième *item*
        ```

    === "Rendu"

        Texte précédant la liste. Notez la ligne blanche avant le début de la liste.

        * premier *item*
        * deuxième *item*

    Les listes ordonnées fonctionnent sur le même principe, on introduit chaque *item* par un `1.` (ou `a.`, au choix).

    === "Markdown"

        ```title=""
        Liste numérotée par des chiffres. On peut se tromper dans les numéros !

        Notez la ligne blanche avant le début de la liste. 

        1. premier *item*
        2. deuxième *item*
        3. troisième *item*
        ```

    === "Rendu"

        Liste numérotée par des chiffres. On peut se tromper dans les numéros !

        Notez la ligne blanche avant le début de la liste. 

        4. premier *item*
        5. deuxième *item*
        6. troisième *item*

??? abstract "Tableaux"

    Les tableaux sont un peu plus laborieux à taper. On trouve toujours une ligne de titre, une deuxième ligne précisant l'alignement puis les données.

    === "Markdown"

        ```title=""
        |   Titre 1    | Titre 2               |               Titre 3 |
        | :----------: | :-------------------- | --------------------: |
        | texte centré | texte aligné à gauche | texte aligné à droite |
        |   ligne 2    | encore la ligne 2     |   toujours la ligne 2 |
        ```

    === "Rendu"

        |   Titre 1    | Titre 2               |               Titre 3 |
        | :----------: | :-------------------- | --------------------: |
        | texte centré | texte aligné à gauche | texte aligné à droite |
        |   ligne 2    | encore la ligne 2     |   toujours la ligne 2 |

??? abstract "Les codes"

    L'un des intérêts de `mkdocs` est la coloration syntaxique automatique :

    * on ouvre une zone de code (en ligne ou en bloc);
    * on précise le langage utilisé
    * on saisit le code
    * on ferme la zone de code

    Les guillemets permettant d'ouvrir les zones de code sont des *backticks* (`alt-gr + 7` puis `espace` sur le clavier).

    === "Markdown"

        ```title=""
        Du texte classique avec un bloc de code *inline* : `print('Hello World')`.
        
        Du texte classique avec un bloc de code *inline* en précisant que c'est du `Python` : `#!py print('Hello World')`.

        Un bloc de code `Python`:

            ```python
            def bonjour(prenom):
                print("Bonjour", prenom)
            ```

        Un bloc de code `javascript`:

            ```javascript
            function bonjour(prenom) {
                console.log("Bonjour", prenom)
            }
            ```
        ```

    === "Rendu"

        Du texte classique avec un bloc de code *inline* : `print('Hello World')`.
        
        Du texte classique avec un bloc de code *inline* en précisant que c'est du `Python` : `#!py print('Hello World')`.

        Un bloc de code `Python`:

        ```python
        def bonjour(prenom):
            print("Bonjour", prenom)
        ```

        Un bloc de code `javascript`:

        ```javascript
        function bonjour(prenom) {
            console.log("Bonjour", prenom)
        }
        ```

??? tip "Aller plus loin"

    On pourra découvrir d'autres fonctionnalités en visitant les sites suivants :

    * [Site officiel de `mkdocs`](https://www.mkdocs.org/)
    
    * [Site officiel de `mkdocs-material`](https://squidfunk.github.io/mkdocs-material/)

    * [Tutoriel de Mireille Coilhac](https://tutoriels.forge.aeif.fr/mkdocs-pyodide-review/)