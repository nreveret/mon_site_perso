Ce texte est affiché une fois que l'utilisateur a saisi un code qui passe tous les tests ou lorsqu'il a essayé plusieurs fois sans succès.

On peut par exemple indiquer des aides ou fournir d'autres versions :

```python
def bonjour(prenom):
    return f"Bonjour {prenom}"
```